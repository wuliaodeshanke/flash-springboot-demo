package com.flash.flashspringbootdemo.controller;

import com.flash.flashspringbootdemo.bean.DemoBean;
import com.flash.flashspringbootdemo.service.ConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
// 只有在括号中的bean在容器中时，才将该controller放入容器
@ConditionalOnBean(ConditionService.class)
public class ConditionController {

    @Autowired
    private DemoBean demoBean;

    @RequestMapping("/hello")
    @ResponseBody
    public String hello() {
        demoBean.invoke();
        return "hello world";
    }

}
