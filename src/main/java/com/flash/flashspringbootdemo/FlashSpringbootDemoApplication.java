package com.flash.flashspringbootdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 这个注解相当于三个注解的叠加
 * @Configuration 标明该类使用Spring基于Java的配置，而不是基于XML
 * @ComponentScan 启用组件扫描，如可以扫描到Controller类
 * @EnableAutoConfiguration 开启自动配置
 */
@SpringBootApplication
public class FlashSpringbootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlashSpringbootDemoApplication.class, args);
	}

}
