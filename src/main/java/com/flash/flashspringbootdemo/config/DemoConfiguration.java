package com.flash.flashspringbootdemo.config;

import com.flash.flashspringbootdemo.bean.DemoBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DemoConfiguration {

    @Bean
    public DemoBean getDemoBean(){
        return new DemoBean();
    }

}
